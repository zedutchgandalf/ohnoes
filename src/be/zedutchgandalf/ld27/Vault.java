package be.zedutchgandalf.ld27;

import be.zedutchgandalf.gdxUtils.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;

public class Vault extends GameScreen {
	private final Sprite backLayer1, backLayer2, backLayer3, backLayer4, controls, jumpToStart, jumpToContinue, dead, decal;
	private final PlayerEntity player;
	private float timeLeft;
	private boolean done, start;
	private float startBuffer;

	public Vault() {
		Physics2D.setMeter(120);
		Entity.setDefaultBindToScreen(true);
		Entity.removeAllEntities();
		Text.init(spriteBatch);
		Text.setColor(Color.BLACK);
		setBackgroundColor(Color.WHITE);
		//		background = new Sprite(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), "office");
		timeLeft = 10f;
		decal = new Sprite(0, 0, 1280, 720, "copDecal");
		dead = new Sprite(-1, -255, 410, 180, "deadMsg");
		controls = new Sprite(10, 10, 310, 140, "controls");
		jumpToStart = new Sprite(-1, -365, 360, 60, "jts");
		jumpToContinue = new Sprite(-1, -365, 360, 60, "jtc");
		backLayer1 = new Sprite(-1, -255, 550, 180, Color.BLACK).setFilled(true);
		backLayer2 = new Sprite(-1, -250, 540, 170).setFilled(true);
		backLayer3 = new Sprite(-1, -365, 360, 60, Color.BLACK).setFilled(true);
		backLayer4 = new Sprite(-1, -360, 350, 50).setFilled(true);
		player = new PlayerEntity(1100, 50);
		new Entity(510, 10, "vault").setWeight(340);
		new Entity(150, 0, "goldBag").setWeight(50);
		new Entity(550, 280, "goldBar");
		new Entity(650, 280, "goldBar");
		new Entity(600, 315, "goldBar");
		for (Entity e : Entity.getAllEntities())
			e.setUpperBound(Gdx.graphics.getHeight() * 1.8f);
		startBuffer = 50;
	}

	@Override
	public void update(float delta) {
		InputManager im = Options.gdxGame.getInputManager();
		if (timeLeft > 0 && start) {
			if (!player.crashed) {
				timeLeft -= delta;
				player.setSprinting(im.btnL1 || im.btnR1);
				player.getMovement().x = im.movementX();
				if (im.aPressed()) {
					player.jump();
					startBuffer = 50;
				}
				if (im.xPressed())
					player.pickItemUp();
			} else if (startBuffer > 0) {
				startBuffer--;
			} else {
				done = im.aPressed();
			}
			Entity.updatePhysicsEntities(delta);
		} else if (timeLeft > 0)
			if (startBuffer > 0)
				startBuffer--;
			else
				start = im.aPressed();
	}

	@Override
	public void draw(float delta) {
		//		background.render(spriteBatch, shapeRenderer);
		Entity.renderEntities(spriteBatch, shapeRenderer);
		if (Options.debugRendering)
			Text.render("Pos: " + player.getPosition() + " Vel: " + player.getMovement() + " Scl: " + Options.screenScaler, 30, -10);
		if (timeLeft > 0) {
			if (player.crashed) {
				dead.render(spriteBatch, shapeRenderer);
				jumpToContinue.render(spriteBatch, shapeRenderer);
			} else if (!start) {
				backLayer1.render(spriteBatch, shapeRenderer);
				backLayer2.render(spriteBatch, shapeRenderer);
				Text.render("Oh Noes! The police found out you were", -1, -100, 2f);
				Text.render("robbing a bank's vault and they're coming", -1, -130, 2f);
				Text.render("to arrest you! Quick! You have 10 seconds", -1, -160, 2f);
				Text.render("before they'll be here, kill yourself!", -1, -190, 2f);
				jumpToStart.render(spriteBatch, shapeRenderer);
			}
		}
		if (!start || Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
			controls.render(spriteBatch, shapeRenderer);
		if (timeLeft <= 0) {
			decal.render(spriteBatch, shapeRenderer);
			backLayer1.render(spriteBatch, shapeRenderer);
			backLayer2.render(spriteBatch, shapeRenderer);
			Text.render("Oh Noes!", -1, -100, 3f);
			Text.render("They have caught you!", -1, -180, 2f);
			backLayer3.render(spriteBatch, shapeRenderer);
			backLayer4.render(spriteBatch, shapeRenderer);
			Text.render("Press R to retry.", -1, -320, 2f);
		}
	}

	@Override
	public GameScreen reload() {
		return new Vault();
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public GameScreen getNextScreen() {
		return new Jail();
	}
}
