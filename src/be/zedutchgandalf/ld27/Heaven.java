package be.zedutchgandalf.ld27;

import be.zedutchgandalf.gdxUtils.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;

public class Heaven extends GameScreen {
	private Sprite background;
	private final Sprite creditsbg;
	private Sprite title;
	private Sprite madeBy;
	private final Sprite ground;
	private final Sprite[] clouds;
	private final Sprite backLayer1, backLayer2, backLayer3, backLayer4, controls, jumpToStart;
	private PlayerEntity player;
	private boolean done, start, credits, switchedToCredits, clickToRestart;
	private float startBuffer, creditTimer;

	public Heaven() {
		Physics2D.setMeter(120);
		Entity.setDefaultBindToScreen(true);
		Entity.removeAllEntities();
		Text.init(spriteBatch);
		Text.setColor(Color.BLACK);
		setBackgroundColor(Color.WHITE);
		background = new Sprite(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), "heaven");
		creditsbg = new Sprite(0, -2 * Gdx.graphics.getHeight(), Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), "sky");
		controls = new Sprite(10, 10, 310, 140, "controls");
		jumpToStart = new Sprite(-1, -365, 360, 60, "jts");
		backLayer1 = new Sprite(-1, -255, 630, 180, Color.BLACK).setFilled(true);
		backLayer2 = new Sprite(-1, -250, 620, 170).setFilled(true);
		backLayer3 = new Sprite(-1, -2365, 360, 60, Color.BLACK).setFilled(true);
		backLayer4 = new Sprite(-1, -2360, 350, 50).setFilled(true);
		player = new PlayerEntity(50, 110);
		clouds = new Sprite[7];
		clouds[0] = new Sprite((float) (Math.random() * Gdx.graphics.getWidth()), -1500, "cloud1");
		clouds[1] = new Sprite((float) (Math.random() * Gdx.graphics.getWidth()), -2500, "cloud2");
		clouds[2] = new Sprite((float) (Math.random() * Gdx.graphics.getWidth()), -3600, "cloud3");
		clouds[3] = new Sprite((float) (Math.random() * Gdx.graphics.getWidth()), -4500, "cloud4");
		clouds[4] = new Sprite((float) (Math.random() * Gdx.graphics.getWidth()), -5800, "cloud5");
		clouds[5] = new Sprite((float) (Math.random() * Gdx.graphics.getWidth()), -6500, "cloud6");
		clouds[6] = new Sprite((float) (Math.random() * Gdx.graphics.getWidth()), -8000, "cloud7");
		title = new Sprite(440, -2000, "title");
		madeBy = new Sprite(220, -2000, "madeBy");
		ground = new Sprite(0, -2001, "ground");
		new Entity(0, 0, 1125, 100, Color.ORANGE).disablePhysics();
		new Entity(100, -250, "cloud1").disablePhysics();
		new Entity(650, 200, "cloud2").disablePhysics();
		for (Entity e : Entity.getAllEntities())
			e.setUpperBound(Gdx.graphics.getHeight() * 1.8f);
		startBuffer = 50;
	}

	@Override
	public void update(float delta) {
		if (!credits) {
			InputManager im = Options.gdxGame.getInputManager();
			if (start) {
				if (!player.crashed) {
					player.setSprinting(im.btnL1 || im.btnR1);
					player.getMovement().x = im.movementX();
					if (im.aPressed()) {
						player.jump();
						startBuffer = 50;
					}
					if (im.xPressed())
						player.pickItemUp();
				} else if (startBuffer > 0) {
					startBuffer--;
				} else {
					done = im.aPressed();
				}
				Entity.updatePhysicsEntities(delta);
			} else if (startBuffer > 0)
				startBuffer--;
			else
				start = im.aPressed();
			if (player.getPosition().y == 0)
				credits = true;
		} else {
			if (!switchedToCredits) {
				switchedToCredits = true;
				float x = player.getPosition().x;
				float y = player.getPosition().y;
				Entity.removeAllEntities();
				player = new PlayerEntity(x, y);
			}
			if (!player.crashed && player.getPosition().y != Gdx.graphics.getHeight() / 2 - player.getSize().y / 2) {
				player.getBoundingBox().y += 1.5f;
				player.updateSpritePosition();
			}
			if (background != null) {
				background.getPosition().y += 50;
				creditsbg.getPosition().y += 40;
				if (background.getPosition().y > Gdx.graphics.getHeight()) {
					background = null;
					creditsbg.getPosition().y = 0;
				}
			} else {
				creditTimer += delta;
				float dx = Options.gdxGame.getInputManager().movementX();
				if (!player.crashed && player.getBoundingBox().x + dx > 0 && player.getBoundingBox().x + dx + player.getSize().x < Gdx.graphics.getWidth()) {
					player.getBoundingBox().x += dx;
					player.updateSpritePosition();
				}
				if (title != null && creditTimer > 4f && title.getPosition().y != 450) {
					title.getPosition().y += 10;
				} else if (title != null && creditTimer > 10f)
					title.getPosition().y += 10;
				if (title != null && title.getPosition().y > Gdx.graphics.getHeight())
					title = null;

				if (madeBy != null && creditTimer > 12f && madeBy.getPosition().y != 270) {
					madeBy.getPosition().y += 10;
				} else if (madeBy != null && creditTimer > 20f)
					madeBy.getPosition().y += 10;
				if (madeBy != null && madeBy.getPosition().y > Gdx.graphics.getHeight())
					madeBy = null;

				if (creditTimer > 25f && ground.getPosition().y < -1)
					ground.getPosition().y += 10;
				else if (ground.getPosition().y == -1) {
					player.getBoundingBox().y -= 10;
					if (ground.intersect(player.getBoundingBox())) {
						player.crashed();
						player.getBoundingBox().y = 100;
						player.updateSpritePosition();
						ground.getPosition().y = 0;
					}
				}

				if (creditTimer > 30f && backLayer3.getPosition().y < 0 && player.crashed) {
					backLayer3.getPosition().y += 2000;
					backLayer4.getPosition().y += 2000;
					clickToRestart = true;
				}

				if (clickToRestart && Options.gdxGame.getInputManager().aPressed())
					done = true;

				if (ground.getPosition().y < -1)
					for (Sprite e : clouds) {
						e.getPosition().y += 50;
						if (e.getPosition().y > Gdx.graphics.getHeight())
							e.getPosition().y = (float) (-10000 * Math.random());
					}
			}
		}
	}

	@Override
	public void draw(float delta) {
		if (credits)
			creditsbg.render(spriteBatch, shapeRenderer);
		if (background != null)
			background.render(spriteBatch, shapeRenderer);
		if (title != null)
			title.render(spriteBatch, shapeRenderer);
		if (madeBy != null)
			madeBy.render(spriteBatch, shapeRenderer);
		for (Sprite e : clouds)
			e.render(spriteBatch, shapeRenderer);
		ground.render(spriteBatch, shapeRenderer);
		Entity.renderEntities(spriteBatch, shapeRenderer);
		backLayer3.render(spriteBatch, shapeRenderer);
		backLayer4.render(spriteBatch, shapeRenderer);
		if (Options.debugRendering)
			Text.render("Pos: " + player.getPosition() + " Vel: " + player.getMovement() + " dt: " + creditTimer, 30, -10);
		if (!start) {
			backLayer1.render(spriteBatch, shapeRenderer);
			backLayer2.render(spriteBatch, shapeRenderer);
			Text.render("Oh Noes! You are dead! D:", -1, -100, 2f);
			Text.render("And there's not much you can do about that.", -1, -130, 2f);
			Text.render("Don't hurry! You have eternity left before", -1, -160, 2f);
			Text.render("anything happens, and you can't kill yourself!", -1, -190, 2f);
			jumpToStart.render(spriteBatch, shapeRenderer);
		}
		if (clickToRestart) {
			Text.render("Jump to restart the game", -1, -320, 2f);
		}
		if (!start || Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
			controls.render(spriteBatch, shapeRenderer);
	}

	@Override
	public GameScreen reload() {
		return new Heaven();
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public GameScreen getNextScreen() {
		return new Office();
	}
}
