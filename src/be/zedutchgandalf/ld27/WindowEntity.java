package be.zedutchgandalf.ld27;

import be.zedutchgandalf.gdxUtils.Entity;
import be.zedutchgandalf.gdxUtils.StateDrivenSprite;

public class WindowEntity extends Entity {
	private boolean broken;

	public WindowEntity(float x, float y, String textureNormal, String textureBroken) {
		super(x, y, textureNormal);
		this.sprite = new StateDrivenSprite(x, y, textureNormal, textureBroken);
		boundingBox.set(sprite.getPosition().x, sprite.getPosition().y, sprite.getWidth(), sprite.getHeight());
		speed = 0;
		disablePhysics();
		setWeight(500);
		controllable = false;
		setSolid(false);
		allEntities.add(this);
		updateSpritePosition();
	}

	public boolean isBroken() {
		return broken;
	}

	@Override
	public void hitBy(Entity e) {
		if (!(e instanceof PlayerEntity) && !broken) {
			((StateDrivenSprite) sprite).setState(1);
			broken = true;
			System.out.println("Window shatters");
		} else if ((e instanceof PlayerEntity) && broken) {
			System.out.println("Player jumped out of the window");
			e.ranInto(this);
			if (e.getEntityCarrying() != null)
				e.getEntityCarrying().destroy();
			e.destroy();
		} else if (broken) {
			e.destroy();
		}
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		for (Entity e : Entity.solidEntities) {
			if ((e == null || e.equals(this) || e.getMovement().x < 10 || e.getWeight() < 25) && !(e instanceof PlayerEntity)) {
				continue;
			}
			if (e instanceof PlayerEntity && this.intersect(e) && !e.isOnGround())
				hitBy(e);
			else if (this.intersect(e) && !(e instanceof PlayerEntity)) {
				e.ranInto(this);
			}
		}
	}
}
