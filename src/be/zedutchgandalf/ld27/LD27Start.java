package be.zedutchgandalf.ld27;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;

public class LD27Start {
	public static void main(String[] args) throws Exception {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "OHNOES";
		config.vSyncEnabled = true;
		config.useGL20 = true;
		config.resizable = false;
		config.backgroundFPS = -1;
		config.foregroundFPS = 60;
		config.initialBackgroundColor = Color.BLACK;
		config.width = 1280;
		config.height = 720;
		new LwjglApplication(new LD27Game(), config);
	}
}
