package be.zedutchgandalf.ld27;

import be.zedutchgandalf.gdxUtils.ControllableEntity;
import be.zedutchgandalf.gdxUtils.Entity;
import be.zedutchgandalf.gdxUtils.ResourceManager;
import be.zedutchgandalf.gdxUtils.StateDrivenSprite;

import com.badlogic.gdx.graphics.Color;

public class PlayerEntity extends ControllableEntity {

	public boolean crashed;

	public PlayerEntity(float x, float y) {
		super(x, y, ResourceManager.getTexture("player").getWidth(), ResourceManager.getTexture("player").getHeight());
		sprite = new StateDrivenSprite(x, y, "player", "playerDead");
		sprite.setColor(Color.BLACK);
		bindToScreen();
		setWeight(50);
	}

	@Override
	public void crashed() {
		((StateDrivenSprite) sprite).setState(1);
		boundingBox.height = sprite.getHeight();
		updateSpritePosition();
		crashed = true;
		sprite.setColor(Color.RED);
		System.out.println("Dear diary, today I fell " + fallingHeight);
		super.crashed();
	}

	@Override
	public void crushedBy(Entity e) {
		if (e.getWeight() >= 0.5f * weight) {
			((StateDrivenSprite) sprite).setState(1);
			boundingBox.height = sprite.getHeight();
			updateSpritePosition();
			e.forceSetOnGround(false);
			System.out.println("Player crushed by " + e);
			crashed = true;
			sprite.setColor(Color.RED);
		}
		super.crushedBy(e);
	}

	@Override
	public void ranInto(Entity e) {
		if (e.getWeight() >= 1.5f * weight) {
			((StateDrivenSprite) sprite).setState(1);
			boundingBox.height = sprite.getHeight();
			updateSpritePosition();
			System.out.println("Player killed by " + e);
			crashed = true;
			sprite.setColor(Color.RED);
		}
	}

	@Override
	public void update(float delta) {
		super.update(delta);
	}
}
