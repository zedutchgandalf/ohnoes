package be.zedutchgandalf.ld27;

import be.zedutchgandalf.gdxUtils.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;

public class Office extends GameScreen {
	//	private final Sprite background;
	private final Sprite officeStart, controls, jumpToStart, jumpToContinue, dead, decal, backLayer1, backLayer2, backLayer3, backLayer4;
	private float timeLeft;
	private final PlayerEntity player;
	private final WindowEntity window;
	private boolean done, start;
	private float startBuffer;

	public Office() {
		Physics2D.setMeter(120);
		Entity.setDefaultBindToScreen(true);
		Entity.removeAllEntities();
		Text.init(spriteBatch);
		Text.setColor(Color.BLACK);
		setBackgroundColor(Color.WHITE);
		//		background = new Sprite(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), "office");
		timeLeft = 10f;
		decal = new Sprite(0, 0, 1280, 720, "bossDecal");
		dead = new Sprite(-1, -255, 410, 180, "deadMsg");
		officeStart = new Sprite(-1, -255, 550, 180, "officeInfo");
		controls = new Sprite(10, 10, 310, 140, "controls");
		jumpToStart = new Sprite(-1, -365, 360, 60, "jts");
		jumpToContinue = new Sprite(-1, -365, 360, 60, "jtc");
		backLayer1 = new Sprite(-1, -255, 410, 180, Color.BLACK).setFilled(true);
		backLayer2 = new Sprite(-1, -250, 400, 170).setFilled(true);
		backLayer3 = new Sprite(-1, -365, 360, 60, Color.BLACK).setFilled(true);
		backLayer4 = new Sprite(-1, -360, 350, 50).setFilled(true);
		//		backLayer5 = new Sprite(-1, -255, 550, 180, Color.BLACK).setFilled(true);
		//		backLayer6 = new Sprite(-1, -250, 540, 170).setFilled(true);
		//		backLayer7 = new Sprite(10, 10, 310, 180, Color.BLACK).setFilled(true);
		//		backLayer8 = new Sprite(15, 15, 300, 170).setFilled(true);
		window = new WindowEntity(405, 390, "window", "windowBroken");
		player = new PlayerEntity(1100, 50);
		new Entity(300, 0, "desk").setWeight(200);
		new Entity(600, 240, "computer").setWeight(30);
		new Entity(0, 0, "drawer").setWeight(121);
		new Entity(800, 240, "cup");
		for (Entity e : Entity.getAllEntities())
			e.setUpperBound(Gdx.graphics.getHeight() * 1.8f);
		startBuffer = 50;
	}

	@Override
	public void update(float delta) {
		InputManager im = Options.gdxGame.getInputManager();
		if (timeLeft > 0 && start) {
			if (!player.crashed) {
				timeLeft -= delta;
				player.setSprinting(im.btnL1 || im.btnR1);
				player.getMovement().x = im.movementX();
				if (im.aPressed()) {
					player.jump();
					startBuffer = 50;
				}
				if (im.xPressed())
					player.pickItemUp();
			} else if (startBuffer > 0) {
				startBuffer--;
			} else {
				done = im.aPressed();
			}
			window.update(delta);
			Entity.updatePhysicsEntities(delta);
		} else if (timeLeft > 0)
			if (startBuffer > 0)
				startBuffer--;
			else
				start = im.aPressed();
	}

	@Override
	public void draw(float delta) {
		//		background.render(spriteBatch, shapeRenderer);
		Entity.renderEntities(spriteBatch, shapeRenderer);
		if (Options.debugRendering) {
			Text.render("Pos: " + player.getPosition() + " Vel: " + player.getMovement() + " Scl: " + Options.screenScaler, 30, -10);
			Text.render("Windows: " + window.getPosition(), 30, -40);
		}
		if (timeLeft > 0) {
			if (player.crashed) {
				dead.render(spriteBatch, shapeRenderer);
				jumpToContinue.render(spriteBatch, shapeRenderer);
				//			backLayer1.render(spriteBatch, shapeRenderer);
				//			backLayer2.render(spriteBatch, shapeRenderer);
				//			Text.render("Congratulations!", -1, -100, 3f);
				//			Text.render("You died!", -1, -180, 2f);
				//			backLayer3.render(spriteBatch, shapeRenderer);
				//			backLayer4.render(spriteBatch, shapeRenderer);
				//			Text.render("Jump to continue", -1, -320, 2f);
			} else if (!start) {
				officeStart.render(spriteBatch, shapeRenderer);
				jumpToStart.render(spriteBatch, shapeRenderer);
				//			backLayer5.render(spriteBatch, shapeRenderer);
				//			backLayer6.render(spriteBatch, shapeRenderer);
				//			Text.render("Oh Noes! Your boss found out you were", -1, -100, 2f);
				//			Text.render("watching porn at work and he's coming", -1, -130, 2f);
				//			Text.render("to fire you! Quick! You have 10 seconds", -1, -160, 2f);
				//			Text.render("before he'll be here, kill yourself!", -1, -190, 2f);
				//			backLayer3.render(spriteBatch, shapeRenderer);
				//			backLayer4.render(spriteBatch, shapeRenderer);
				//			Text.render("Jump to start", -1, -320, 2f);
				//			backLayer7.render(spriteBatch, shapeRenderer);
				//			backLayer8.render(spriteBatch, shapeRenderer);
				//			Text.render("Jump: W, Z, UP or SPACE", 30, 170, 1.5f);
				//			Text.render("Take/Throw items: X", 30, 130, 1.5f);
				//			Text.render("Sprint: SHIFT, L1, R1", 30, 90, 1.5f);
				//			Text.render("Reload level: R", 30, 50, 1.5f);
			}
		}
		if (!start || Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
			controls.render(spriteBatch, shapeRenderer);
		if (timeLeft <= 0) {
			decal.render(spriteBatch, shapeRenderer);
			backLayer1.render(spriteBatch, shapeRenderer);
			backLayer2.render(spriteBatch, shapeRenderer);
			Text.render("Oh Noes!", -1, -100, 3f);
			Text.render("He has caught you!", -1, -180, 2f);
			backLayer3.render(spriteBatch, shapeRenderer);
			backLayer4.render(spriteBatch, shapeRenderer);
			Text.render("Press R to retry.", -1, -320, 2f);
		}
	}

	@Override
	public GameScreen reload() {
		return new Office();
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public GameScreen getNextScreen() {
		return new Bedroom();
	}
}
