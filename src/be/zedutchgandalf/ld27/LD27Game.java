package be.zedutchgandalf.ld27;

import be.zedutchgandalf.gdxUtils.GdxGame;
import be.zedutchgandalf.gdxUtils.Options;
import be.zedutchgandalf.gdxUtils.ResourceManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

public class LD27Game extends GdxGame {

	@Override
	protected void init() {
		Options.physicsOnByDefault = true;
		Options.enableFullscreen = false;
		ResourceManager.loadTexture("player", "player.png");
		ResourceManager.loadTexture("playerDead", "player_dead.png");
		ResourceManager.loadTexture("office", "office.png");
		ResourceManager.loadTexture("desk", "desk.png");
		ResourceManager.loadTexture("computer", "computer.png");
		ResourceManager.loadTexture("cup", "cup.png");
		ResourceManager.loadTexture("drawer", "drawer.png");
		ResourceManager.loadTexture("bed", "bed.png");
		ResourceManager.loadTexture("kast", "kastje.png");
		ResourceManager.loadTexture("flower", "flowerVase.png");
		ResourceManager.loadTexture("vase", "vase.png");
		ResourceManager.loadTexture("wall", "wall.png");
		ResourceManager.loadTexture("window", "window.png");
		ResourceManager.loadTexture("windowBroken", "window_broken.png");
		ResourceManager.loadTexture("potato", "potato.png");
		ResourceManager.loadTexture("garbage", "garbageCan.png");
		ResourceManager.loadTexture("vault", "vault.png");
		ResourceManager.loadTexture("goldBar", "gold.png");
		ResourceManager.loadTexture("goldBag", "bag.png");
		ResourceManager.loadTexture("jail", "jail.png");
		ResourceManager.loadTexture("table", "table.png");
		ResourceManager.loadTexture("bowl", "bowl.png");
		ResourceManager.loadTexture("bench", "bench.png");
		ResourceManager.loadTexture("heaven", "heaven.png");
		ResourceManager.loadTexture("cloud1", "cloud1.png");
		ResourceManager.loadTexture("cloud2", "cloud2.png");
		ResourceManager.loadTexture("cloud3", "cloud3.png");
		ResourceManager.loadTexture("cloud4", "cloud4.png");
		ResourceManager.loadTexture("cloud5", "cloud5.png");
		ResourceManager.loadTexture("cloud6", "cloud6.png");
		ResourceManager.loadTexture("cloud7", "cloud7.png");
		ResourceManager.loadTexture("sky", "sky.png");
		ResourceManager.loadTexture("title", "title.png");
		ResourceManager.loadTexture("madeBy", "credits.png");
		ResourceManager.loadTexture("ground", "ground.png");
		ResourceManager.loadTexture("controls", "controls.png");
		ResourceManager.loadTexture("officeInfo", "office_start.png");
		ResourceManager.loadTexture("bedroomInfo", "bedroom_start.png");
		ResourceManager.loadTexture("deadMsg", "dead.png");
		ResourceManager.loadTexture("jtc", "jumpToContinue.png");
		ResourceManager.loadTexture("jts", "jumpToStart.png");
		ResourceManager.loadTexture("bossDecal", "boss_decal.png");
		ResourceManager.loadTexture("copDecal", "police_decal.png");
		ResourceManager.loadTexture("wifeDecal", "wife_decal.png");
		startScreen(new Office());
	}

	@Override
	public void OpenGLUpdating() {
		if (Gdx.input.isKeyPressed(Input.Keys.R))
			setScreen(getScreen().reload());
		super.OpenGLUpdating();
	}
}